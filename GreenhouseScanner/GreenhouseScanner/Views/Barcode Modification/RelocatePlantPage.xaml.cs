﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using GreenhouseScanner.Models;

namespace GreenhouseScanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RelocatePlantPage : ContentPage
    {
        RelocateBarcodeModel relocator = new RelocateBarcodeModel();
        public RelocatePlantPage()
        {
            InitializeComponent();
            PopulatePicker();
            submitBtn.Clicked += SubmitBtn_Clicked;
        }

        private async void SubmitBtn_Clicked(object sender, EventArgs e)
        {
            int.TryParse(barcodeEntry.Text, out relocator.Barcode);
            relocator.Location = LocationModel.LocationNumberByName(locationPkr.SelectedItem.ToString());
            var result = await relocator.RelocateAsynce();
            await DisplayAlert("Result", result, "OK");
        }

        private void PopulatePicker()
        {
            foreach (var item in LocationModel.LocationDict)
            {
                locationPkr.Items.Add(item.Value);
            }
        }
    }
}
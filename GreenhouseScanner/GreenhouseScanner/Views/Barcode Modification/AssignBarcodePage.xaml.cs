﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using GreenhouseScanner.Models;

namespace GreenhouseScanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AssignBarcodePage : ContentPage
    {
        AssignBarcodeModel assignment = new AssignBarcodeModel();
        public AssignBarcodePage()
        {
            InitializeComponent();
            submitBtn.Clicked += SubmitBtn_Clicked;
        }

        private async void SubmitBtn_Clicked(object sender, EventArgs e)
        {
            int.TryParse(barcodeEntry.Text, out assignment.Barcode);
            int.TryParse(idEntry.Text, out assignment.PlantID);
            assignment.Location = loacationPkr.SelectedIndex;

            //TODO Code for inserting Access Code
            var result = await assignment.AssignAsync();
            await DisplayAlert("Result", result, "OK");
        }

        public void reset()
        {
            barcodeEntry.Text = string.Empty;
            idEntry.Text = string.Empty;
        }
    }
}
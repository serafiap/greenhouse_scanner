﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using GreenhouseScanner.Models;

namespace GreenhouseScanner.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UnassignBarcodePage : ContentPage
	{
        UnassignBarcodeModel unassigner = new UnassignBarcodeModel();
		public UnassignBarcodePage ()
		{
			InitializeComponent ();
            submitBtn.Clicked += SubmitBtn_Clicked;
		}

        private async void SubmitBtn_Clicked(object sender, EventArgs e)
        {
            int.TryParse(barcodeEntry.Text, out unassigner.Barcode);
            var result = await unassigner.UnassignAsync();
            await DisplayAlert("Result", result, "OK");
        }
    }
}
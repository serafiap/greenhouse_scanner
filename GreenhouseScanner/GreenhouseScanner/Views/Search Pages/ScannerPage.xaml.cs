﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GreenhouseScanner.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net;
using System.Text;

namespace GreenhouseScanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScannerPage : ContentPage
    {
        CompletePlantInformation selectedPlant = null;

        public ScannerPage() : this(null, null)
        {
            
        }
        public ScannerPage(CompletePlantInformation plant, List<string> barcodes)
        {
            InitializeComponent();
            selectedPlant = plant;
            scanBtn.Clicked += ScanBtn_Clicked;
            //statusBtn.Clicked += StatusBtn_Clicked;
            carePageBtn.Clicked += CarePageBtn_Clicked;
            //DownloadData();

            if(selectedPlant != null)
            {
                if (barcodes.Count > 0)
                    barcodeLbl.Text = string.Join("\n", barcodes);
                else
                    barcodeLbl.Text = "No active barcodes";
                PopulatePlantInfoFields();
            }
        }

        private async void CarePageBtn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CareInstructionsPage(selectedPlant));
        }

        //private void StatusBtn_Clicked(object sender, EventArgs e)
        //{
        //    //DownloadData();
        //}

        private async void ScanBtn_Clicked(object sender, EventArgs e)
        {
            clearLabels();
            
            var id = await new BarcodeScanner().Scan();

            selectedPlant = null;

            var selectedPot = InventoryEntry.CurrentInventoryEntryList.Where(i => i.Barcode == id).FirstOrDefault();

            if (selectedPot != null)
                selectedPlant = CompletePlantInformation.PlantCareList.Where(i => i.ID == selectedPot.PlantID).FirstOrDefault();

            if (selectedPlant != null)
            {
                barcodeLbl.Text = id;
                PopulatePlantInfoFields();
            }
            else
            {
                carePageBtn.IsEnabled = false;
                barcodeLbl.Text = id;
                plantIdLbl.Text = "No Entry Found";
            }
        }

        private void PopulatePlantInfoFields()
        {
            StringBuilder formattedCommonNames = new StringBuilder();
            var names = selectedPlant.CommonNames.Split(';');
            foreach (var name in names)
            {
                formattedCommonNames.Append(name + "\n");
            }
            scientificNameLbl.Text = string.Format("{0} {1}", selectedPlant.Genus, selectedPlant.Species);
            commonNamesLbl.Text = formattedCommonNames.ToString();
            plantIdLbl.Text = selectedPlant.ID;
            carePageBtn.IsEnabled = true;
        }

        private void clearLabels()
        {
            barcodeLbl.Text = string.Empty;
            plantIdLbl.Text = string.Empty;
            scientificNameLbl.Text = string.Empty;
            commonNamesLbl.Text = string.Empty;
        }

        //private async void DownloadData()
        //{
        //    statusBtn.IsEnabled = false;
        //    statusBtn.BackgroundColor = Color.Default;
        //    statusBtn.Text = "Data Downloading...";
        //        await DatabaseData.GetCurrentData();
            
        //    if (DatabaseData.IsDataLoaded())
        //    {
        //        statusBtn.Text = "Data Successfully Loaded";
        //        statusBtn.BackgroundColor = Color.LightGreen;
        //        statusBtn.IsEnabled = true;
        //    }
        //    else
        //    {
        //        statusBtn.Text = "Error loading data. Click to retry.";
        //        statusBtn.BackgroundColor = Color.Pink;
        //        statusBtn.IsEnabled = true;
        //    }
        //}




    }
}
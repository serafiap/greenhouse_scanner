﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using GreenhouseScanner.Models;

namespace GreenhouseScanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlantSearchResultPage : ContentPage
    {
        public PlantSearchResultPage(List<CompletePlantInformation> potentialPlants)
        {
            InitializeComponent();

            foreach (var plant in potentialPlants)
            {

                string commonNames = string.Join(", ", plant.CommonNames.Split(';'));
                Button button = new Button()
                {
                    Text = string.Format("{0} {1}\n{2}", plant.Genus, plant.Species, commonNames),
                    
                };
                button.Clicked += (object sender, EventArgs e) => Button_Clicked(sender, e, plant);

                layout.Children.Add(button);
            }
            
        }

        private void Button_Clicked(object sender, EventArgs e, CompletePlantInformation plant)
        {
            List<string> matchingBarcodes = new List<string>();
            List<InventoryEntry> matchingInventory = InventoryEntry.CurrentInventoryEntryList.Where(i => i.PlantID == plant.ID).ToList();
            foreach (var entry in matchingInventory)
            {
                matchingBarcodes.Add(string.Format("{0} - {1}", entry.Barcode, entry.LocationName()));
            }
            Navigation.PushAsync(new ScannerPage(plant, matchingBarcodes));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using GreenhouseScanner.Models;

namespace GreenhouseScanner.Views
{
    using CPI = CompletePlantInformation;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CommonNameSearchPage : ContentPage
    {
        public CommonNameSearchPage()
        {
            InitializeComponent();
            clearBtn.Clicked += ClearBtn_Clicked;
            searchBtn.Clicked += SearchBtn_Clicked;
        }

        private void SearchBtn_Clicked(object sender, EventArgs e)
        {
            var potentialPlants = CPI.PlantCareList;
            if (nameEntry.Text != null)
            {
                foreach (var namelet in nameEntry.Text.ToLower().Split(' '))
                {
                    potentialPlants = potentialPlants.Where(i => i.CommonNames.ToLower().Contains(namelet)).ToList();
                } 
            }

            Navigation.PushAsync(new PlantSearchResultPage(potentialPlants));
        }

        private void ClearBtn_Clicked(object sender, EventArgs e)
        {
            nameEntry.Text = string.Empty;
        }
    }
}
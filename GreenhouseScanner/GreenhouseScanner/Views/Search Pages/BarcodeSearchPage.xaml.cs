﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using GreenhouseScanner.Models;

namespace GreenhouseScanner.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BarcodeSearchPage : ContentPage
    {
        CompletePlantInformation selectedPlant = null;
        public BarcodeSearchPage ()
		{
			InitializeComponent ();

            searchBtn.Clicked += ScanBtn_Clicked;
            barcodeLbl.Completed += ScanBtn_Clicked;
            carePageBtn.Clicked += CarePageBtn_Clicked;
		}

        private void CarePageBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CareInstructionsPage(selectedPlant));
        }

        private void ScanBtn_Clicked(object sender, EventArgs e)
        {
            string id = barcodeLbl.Text;
            selectedPlant = null;

            clearLabels();

            var selectedPot = InventoryEntry.CurrentInventoryEntryList.Where(i => i.Barcode == id).FirstOrDefault();

            if (selectedPot != null)
                selectedPlant = CompletePlantInformation.PlantCareList.Where(i => i.ID == selectedPot.PlantID).FirstOrDefault();

            if (selectedPlant != null)
            {
                StringBuilder formattedCommonNames = new StringBuilder();
                var names = selectedPlant.CommonNames.Split(';');
                foreach (var name in names)
                {
                    formattedCommonNames.Append(name + "\n");
                }
                scientificNameLbl.Text = string.Format("{0} {1}", selectedPlant.Genus, selectedPlant.Species);
                commonNamesLbl.Text = formattedCommonNames.ToString();
                barcodeLbl.Text = id;
                plantIdLbl.Text = selectedPlant.ID;
                carePageBtn.IsEnabled = true;
            }
            else
            {
                carePageBtn.IsEnabled = false;
                barcodeLbl.Text = id;
                plantIdLbl.Text = "No Entry Found";
            }

            
        }

        private void clearLabels()
        {
            barcodeLbl.Text = string.Empty;
            plantIdLbl.Text = string.Empty;
            scientificNameLbl.Text = string.Empty;
            commonNamesLbl.Text = string.Empty;
        }
    }
}
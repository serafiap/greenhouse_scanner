﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using GreenhouseScanner.Models;

namespace GreenhouseScanner.Views
{
    using CPI = CompletePlantInformation;
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ScientificNameSearchPage : ContentPage
	{
		public ScientificNameSearchPage ()
		{
			InitializeComponent ();
            clearBtn.Clicked += ClearBtn_Clicked;
            searchBtn.Clicked += SearchBtn_Clicked;
		}

        private void SearchBtn_Clicked(object sender, EventArgs e)
        {
            var potentialPlants = CPI.PlantCareList;
            if (genusEntry.Text != null)
            {
                foreach (var namelet in genusEntry.Text.ToLower().Split(' '))
                {
                    potentialPlants = potentialPlants.Where(i => i.Genus.ToLower().Contains(namelet)).ToList();
                }
            }

            if (speciesEntry.Text != null)
            {
                foreach (var namelet in speciesEntry.Text.ToLower().Split(' '))
                {
                    potentialPlants = potentialPlants.Where(i => i.Species.ToLower().Contains(namelet)).ToList();
                }
            }

            Navigation.PushAsync(new PlantSearchResultPage(potentialPlants));
        }

        private void ClearBtn_Clicked(object sender, EventArgs e)
        {
            genusEntry.Text = string.Empty;
            speciesEntry.Text = string.Empty;
        }
    }
}
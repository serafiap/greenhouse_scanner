﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using GreenhouseScanner.Models;

namespace GreenhouseScanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CareInstructionsPage : ContentPage
    {
        private CompletePlantInformation plantInfo = new CompletePlantInformation();
        public CareInstructionsPage(CompletePlantInformation infoToDisplay)
        {
            InitializeComponent();
            plantInfo = infoToDisplay;

            titleLbl.Text = string.Format("Care Information for: \n{0} {1}", infoToDisplay.Genus, infoToDisplay.Species);

            waterLbl.Text = WaterLevelsString(infoToDisplay);
            heightLbl.Text = MaxMeasurementString(infoToDisplay.MaxHeight);
            spreadLbl.Text = MaxMeasurementString(infoToDisplay.MaxSpread);
            propagationLbl.Text = PropagationString(infoToDisplay);
            hazardLbl.Text = HazardsString(infoToDisplay);
        }

        private string WaterLevelsString(CompletePlantInformation info)
        {
            string newline = "";
            StringBuilder sb = new StringBuilder();
            if (Convert.ToBoolean(info.LowWater))
            { sb.Append("Arid Room"); newline = "\n"; }
            if (Convert.ToBoolean(info.MedWater))
            { sb.Append(string.Format("{0}Temperate Room", newline)); newline = "\n"; }
            if (Convert.ToBoolean(info.HighWater))
            { sb.Append(string.Format("{0}Tropical Room", newline)); }
            return sb.ToString();
        }

        private string MaxMeasurementString(string measurement)
        {
            decimal d = Convert.ToDecimal(measurement);

            if (d <= 2.00m)
                return string.Format("{0} inches", (int)(d * 12m));
            else
                return string.Format("{0} feet", (int)d);
        }

        private string PropagationString(CompletePlantInformation info)
        {
            string newline = "";
            StringBuilder sb = new StringBuilder();
            if (Convert.ToBoolean(info.CuttingPropagation))
            { sb.Append("Stem Cuttings"); newline = "\n"; }
            if (Convert.ToBoolean(info.DivisionPropagation))
            { sb.Append(string.Format("{0}Root Division", newline)); newline = "\n"; }
            if (Convert.ToBoolean(info.LeafPropagation))
            { sb.Append(string.Format("{0}Leaf Cuttings", newline)); }
            return sb.ToString();
        }

        private string HazardsString(CompletePlantInformation info)
        {
            string newline = "";
            StringBuilder sb = new StringBuilder();
            if (Convert.ToBoolean(info.Sharp))
            { sb.Append("Sharp"); newline = "\n"; }
            if (Convert.ToBoolean(info.Poisonous))
            { sb.Append(string.Format("{0}Poisonous", newline)); newline = "\n"; }
            if (Convert.ToBoolean(info.Irritant))
            { sb.Append(string.Format("{0}Contact Irritation", newline)); }
            return sb.ToString();
        }
        

    }
}
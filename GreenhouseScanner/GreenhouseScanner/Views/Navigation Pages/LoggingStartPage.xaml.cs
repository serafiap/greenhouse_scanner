﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GreenhouseScanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoggingStartPage : ContentPage
    {
        public LoggingStartPage()
        {
            InitializeComponent();
            accessBtn.Clicked += AccessBtn_Clicked;
            assignBtn.Clicked += AssignBtn_Clicked;
            unassignBtn.Clicked += UnassignBtn_Clicked;
            changeBtn.Clicked += ChangeBtn_Clicked;
            relocateBtn.Clicked += RelocateBtn_Clicked;
        }

        private void AccessBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AssignBarcodePage());
        }

        private void AssignBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AssignBarcodePage());
        }

        private void UnassignBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new UnassignBarcodePage());
        }

        private void ChangeBtn_Clicked(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void RelocateBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RelocatePlantPage());
        }
    }
}
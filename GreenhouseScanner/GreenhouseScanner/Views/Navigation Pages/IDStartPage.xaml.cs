﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using GreenhouseScanner.Models;

namespace GreenhouseScanner.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class IDStartPage : ContentPage
	{
		public IDStartPage ()
		{
			InitializeComponent ();
            DownloadData();
            statusBtn.Clicked += StatusBtn_Clicked;
            scannerBtn.Clicked += ScannerBtn_Clicked;
            barcodeBtn.Clicked += BarcodeBtn_Clicked;
            scientificSearchBtn.Clicked += ScientificSearchBtn_Clicked;
            commonSearchBtn.Clicked += CommonSearchBtn_Clicked;
            currentInventory.Clicked += CurrentInventory_Clicked;
            loggingBtn.Clicked += LoggingBtn_Clicked;
		}

        private void LoggingBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new LoggingStartPage());
        }

        private void CurrentInventory_Clicked(object sender, EventArgs e)
        {
            var activePlantIDs = InventoryEntry.CurrentInventoryEntryList.Select(i => i.PlantID).Distinct().ToList();
            var potentialPlants = CompletePlantInformation.PlantCareList.Where(i => activePlantIDs.Contains(i.ID)).ToList();
            Navigation.PushAsync(new PlantSearchResultPage(potentialPlants));
        }

        private void CommonSearchBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CommonNameSearchPage());
        }

        private void ScientificSearchBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ScientificNameSearchPage());
        }

        private void StatusBtn_Clicked(object sender, EventArgs e)
        {
            DownloadData();
        }

        private void ScannerBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ScannerPage());
        }

        private void BarcodeBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new BarcodeSearchPage());
        }

        private async void DownloadData()
        {
            statusBtn.IsEnabled = false;
            scannerBtn.IsEnabled = false;
            barcodeBtn.IsEnabled = false;
            scientificSearchBtn.IsEnabled = false;
            commonSearchBtn.IsEnabled = false;
            currentInventory.IsEnabled = false;
            loggingBtn.IsEnabled = false;

            statusBtn.BackgroundColor = Color.Default;
            statusBtn.Text = "Data Downloading...";
            await DatabaseData.GetCurrentData();

            if (DatabaseData.IsDataLoaded())
            {
                statusBtn.Text = "Data Successfully Loaded";
                statusBtn.BackgroundColor = Color.LightGreen;
                statusBtn.IsEnabled = true;
                scannerBtn.IsEnabled = true;
                barcodeBtn.IsEnabled = true;
                scientificSearchBtn.IsEnabled = true;
                commonSearchBtn.IsEnabled = true;
                currentInventory.IsEnabled = true;
                //loggingBtn.IsEnabled = true;
            }
            else
            {
                statusBtn.Text = "Error loading data. Click to retry.";
                statusBtn.BackgroundColor = Color.Pink;
                statusBtn.IsEnabled = true;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using GreenhouseScanner.Models;

namespace GreenhouseScanner.Models
{
    public class CompletePlantInformation
    {
        public string ID;
        public string Genus;
        public string Species;
        public string CommonNames;
        public string LowWater;
        public string MedWater;
        public string HighWater;
        public string MaxHeight;
        public string MaxSpread;
        public string CuttingPropagation;
        public string DivisionPropagation;
        public string LeafPropagation;
        public string Poisonous;
        public string Sharp;
        public string Irritant;
        public string SpecialInstructions;

        public static List<CompletePlantInformation> PlantCareList = new List<CompletePlantInformation>();
        
        public async static Task RetrievePlantInformationTable()
        {
            try
            {
                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                var uri = new Uri("http://greenhouse.aaronserafin.net/api/plantinformation/");
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    PlantCareList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CompletePlantInformation>>(content);
                }
            }
            catch
            {
                PlantCareList = new List<CompletePlantInformation>();
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using System.Net.Http;

namespace GreenhouseScanner.Models
{
    class UnassignBarcodeModel
    {
        public int AccessCode = 0;
        public int Barcode = 0;

        public async Task<string> UnassignAsync()
        {
            string json = JsonConvert.SerializeObject(this);
            try
            {
                var content = new FormUrlEncodedContent(new[]{
                    new KeyValuePair<string, string>("", json)
                }
                );
                HttpClient client = new HttpClient();
                var uri = new Uri("http://greenhouse.aaronserafin.net/api/removebarcode/");
                var response = await client.PostAsync(uri, content);
                if (response.IsSuccessStatusCode)
                {
                    var reply = await response.Content.ReadAsStringAsync();
                    return reply.ToString();
                }
                return "0";
            }
            catch
            {
                return "0";
            }
        }
    }
}

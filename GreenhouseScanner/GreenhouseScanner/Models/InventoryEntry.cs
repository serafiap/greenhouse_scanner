﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreenhouseScanner.Models
{
    class InventoryEntry
    {
        public string Barcode;
        public string PlantID;
        public string Location;
        //public string DatePlanted;
        //public string LastRepot;
        //public string LastFertillized;
        //public string Active;

        public static List<InventoryEntry> CurrentInventoryEntryList = new List<InventoryEntry>();

        public async static Task RetrieveCurrentInventoryTable()
        {
            try
            {
                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                var uri = new Uri("http://greenhouse.aaronserafin.net/api/plantinventory/");
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    CurrentInventoryEntryList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<InventoryEntry>>(content);
                }
            }
            catch
            {
                CurrentInventoryEntryList = new List<InventoryEntry>();
            }
        }

        public string LocationName()
        {
            switch (this.Location)
            {
                case "1":
                    return "Arid Room";
                case "2":
                    return "Temperate Room";
                case "3":
                    return "Tropical Room";
                default:
                    return "Unknown Location";
            }
        }
    }
}

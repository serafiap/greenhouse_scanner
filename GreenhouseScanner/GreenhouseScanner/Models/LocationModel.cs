﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreenhouseScanner.Models
{
    class LocationModel
    {
        public static readonly Dictionary<int, string> LocationDict = new Dictionary<int, string>()
        {
            {0, "None" },
            {1, "Arid Room" },
            {2, "Temperate Room" },
            {3, "Temperate Room" },
            {4, "Aerocloner" }
        };

        public static int LocationNumberByName (string name)
        {
            return LocationDict.Where(i => i.Value == name).ToList()[0].Key;
        }
    }
}

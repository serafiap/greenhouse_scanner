﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GreenhouseScanner.Models;

namespace GreenhouseScanner
{
    static class DatabaseData
    {
        public async static Task GetCurrentData()
        {
            try
            {
                await CompletePlantInformation.RetrievePlantInformationTable();
                await InventoryEntry.RetrieveCurrentInventoryTable();
            }
            catch (Exception e)
            {
            }
        }

        public static bool IsDataLoaded()
        {
            if (InventoryEntry.CurrentInventoryEntryList.Count != 0 && CompletePlantInformation.PlantCareList.Count != 0)
                return true;
            return false;
        }

        public static string PlantIDFromBarcode (string barcodeNumber)
        {

            return "";
        }



    }
}

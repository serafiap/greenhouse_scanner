﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZXing.Mobile;

namespace GreenhouseScanner.Models
{
    class BarcodeScanner
    {

        public string Text = "";
        
        public async Task<string> Scan()
        {
            var scanner = new MobileBarcodeScanner();
            var result = await scanner.Scan();

            if (result != null)
                return result.Text;
            return "No barcode";
        }
    }
}
